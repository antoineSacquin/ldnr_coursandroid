package ldnr.monzoo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by stag on 29/06/17.
 */

public class AlertActivity extends Activity {

    private EditText alertText;
    private CheckBox alertUrgent;
    private AutoCompleteTextView alertLocation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alert);

        this.alertText = findViewById(R.id.title_edit_alert);
        this.alertUrgent = findViewById(R.id.urgent_check_alert);
        this.alertLocation = findViewById(R.id.location_edit_alert);

        String[] locations = getResources().getStringArray(R.array.alert_locations);
        int layout = android.R.layout.simple_dropdown_item_1line;

        ArrayAdapter<String> aa = new ArrayAdapter<String>(this, layout, locations);

        this.alertLocation.setAdapter(aa);
    }

    public void send(View view) {

        String msgAlerte = this.alertText.getText().toString();
        String location = this.alertLocation.getText().toString();

        String toastMsg = getString(R.string.alert_toast,
                getString(R.string.alert_title),
                msgAlerte,
                location,
                getString(R.string.alert_sent));

        boolean errors = testForErrors(msgAlerte, location);

        if (!errors) {
            if (this.alertUrgent.isChecked()) {
                toastMsg = getString(R.string.urgent_alert_toast,
                        getString(R.string.alert_title),
                        msgAlerte,
                        location,
                        getString(R.string.alert_sent));
            }
            Toast.makeText(this, toastMsg, Toast.LENGTH_SHORT).show();
        }

    }

    private boolean testForErrors(String msgAlerte, String location) {
        boolean errors = false;

        AlertDialog.Builder adb = new AlertDialog.Builder(this);

        String errosMsg;

        adb.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        if (msgAlerte.isEmpty() && location.isEmpty()) {
            errosMsg = getString(R.string.two_error_alert_toast);
            adb.setMessage(errosMsg);
            adb.show();

        } else if (msgAlerte.isEmpty()) {
            errosMsg = getString(R.string.one_error_alert_toast, getString(R.string.title));
            adb.setMessage(errosMsg);
            adb.show();

        } else if (location.isEmpty()) {
            errosMsg = getString(R.string.one_error_alert_toast, getString(R.string.location));
            adb.setMessage(errosMsg);
            adb.show();
        }
        return errors;
    }
}

package ldnr.monzoo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

/**
 * Created by stag on 27/06/17.
 */

public class PopcornActivity extends Activity {

    long timer2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new PopcornView(this));
        timer2 = System.currentTimeMillis();
        int timer = getIntent().getIntExtra("timer", 0);
        if (timer > 2000) {
            String s=getResources().getQuantityString(R.plurals.second,timer/1000);

            Toast.makeText(this,getString(R.string.popcorn_toast,timer/1000,s), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {

        timer2 = System.currentTimeMillis() - timer2;
        Intent i = new Intent();
        Log.i("Time in PopCOrn", timer2 + " ms");
        i.putExtra("t", (int) timer2);
        setResult(0, i);
        finish();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("geo:43.5,2.0"));
            startActivity(i);
        }
        return true;

    }

    public class PopcornView extends View {
        public PopcornView(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            Bitmap i = BitmapFactory.decodeResource(getResources(), R.drawable.popcorn);
            Affichage.afficher(this,canvas,i);
        }
    }
}

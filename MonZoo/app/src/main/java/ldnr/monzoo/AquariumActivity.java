package ldnr.monzoo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import android.os.Bundle;
import android.support.annotation.Nullable;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import static android.widget.Toast.LENGTH_LONG;

/**
 * Created by stag on 27/06/17.
 */

public class AquariumActivity extends Activity {

    long timer;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new AquariumView(this));
        timer = System.currentTimeMillis();
        Log.i("AquariumActivity", "Nage droit d'vant toi");
        Toast.makeText(this, getString(R.string.aquarium_create_toast), LENGTH_LONG).show();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            Intent i = new Intent(this, PopcornActivity.class);


            timer = System.currentTimeMillis() - timer;
            i.putExtra("timer", (int) timer);
            Log.i("Time in Aquarium", timer + " ms");

            startActivityForResult(i, 0);
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        int time = (data.getIntExtra("t", 0)) / 1000;

        String s=getResources().getQuantityString(R.plurals.second,time);

        Toast.makeText(this, getString(R.string.aquarium_result_toast,time,s), Toast.LENGTH_LONG).show();
        super.onActivityResult(requestCode, resultCode, data);
    }

    public class AquariumView extends View {
        public AquariumView(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            Bitmap i = BitmapFactory.decodeResource(getResources(), R.drawable.aquarium);

            Affichage.afficher(this,canvas,i);


        }
    }
}

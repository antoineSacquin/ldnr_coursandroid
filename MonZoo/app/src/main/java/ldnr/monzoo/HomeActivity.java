package ldnr.monzoo;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by stag on 28/06/17.
 */

public class HomeActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        TextView welcome = findViewById(R.id.welcome_text);
        String appName = getString(R.string.app_name);
        String phoneNumber = getString(R.string.phone_number);
        welcome.setText(getString(R.string.home_layout, appName, phoneNumber));

        TextView tvNews = findViewById(R.id.home_news);

        try (InputStream is = getAssets().open("news.txt");
             BufferedReader br = new BufferedReader(new InputStreamReader(is))) {

            StringBuilder text = new StringBuilder();
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line + "\n");
            }

            tvNews.setText(Html.fromHtml(text.toString()));
        } catch (IOException e) {
            tvNews.setText("Pas de nouvelle récupérée");
            Log.e("Home", "Erreur", e);
        }

        Button btMap = findViewById(R.id.home_go_map);

        btMap.setOnClickListener(this);

        Button btPopcorn = findViewById(R.id.home_go_alert);
        btPopcorn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(HomeActivity.this, AlertActivity.class));
                    }
                });


        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED){
            saveCSV();

        }else{
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    0);
        }
    }

    @Override
    public void onClick(View view) {

        startActivity(new Intent(this, MapActivity.class));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.home, menu);

        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        boolean send = sp.getBoolean("send", false);
        menu.findItem(R.id.send_menu).setChecked(send);

        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {

        switch (item.getItemId()) {

            case R.id.map_menu:
                startActivity(new Intent(this, MapActivity.class));
                return true;

            case R.id.send_menu:

                item.setChecked(!item.isChecked());
                SharedPreferences sp = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putBoolean("send", item.isChecked());
                editor.commit();
                return true;

            default:
                return false;
        }
    }

    private void saveCSV() {


        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File d = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

            try (FileWriter fw = new FileWriter(new File(d,"log.csv"),true)) {

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date = new Date();

                StringBuilder log=new StringBuilder();

                log.append(dateFormat.format(date));
                log.append(",ouverture\n");

                fw.append(log.toString());
                fw.flush();

            } catch (Exception e) {
                Log.e("Home", "Could not save", e);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
            saveCSV();
        }
    }
}

package ldnr.monzoo;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;

/**
 * Created by stag on 28/06/17.
 */

public class Affichage {

    public static void afficher(View view, Canvas canvas, Bitmap i){

        double hc = view.getHeight();
        double wc = view.getWidth();

        double hi = i.getHeight();
        double wi = i.getWidth();

        double ratio = hi / wi;

        if (hi > hc) {
            hi = hc;
            wi = hi / ratio;
        }
        if (wi > wc) {
            wi = wc;
            hi = ratio * wi;
        }

        int xpos = (int) ((hc - hi) / 2);
        int ypos = (int) ((wc - wi) / 2);


        canvas.drawBitmap(Bitmap.createScaledBitmap(i, (int) wi, (int) hi, false), ypos, xpos, null);
    }
}

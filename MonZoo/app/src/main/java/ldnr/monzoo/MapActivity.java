package ldnr.monzoo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

/**
 * Created by stag on 26/06/17.
 */

public class MapActivity extends Activity implements View.OnClickListener{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(new MapView(this));
        setContentView(R.layout.zoo_map);
        Log.i("MapActivity", "ça roule, bande de moules?");
        Toast.makeText(this, getString(R.string.map_toast), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        startActivity(new Intent(this,AquariumActivity.class));
    }

    /*@Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            Intent i = new Intent(this, AquariumActivity.class);
            startActivity(i);
        }

        return true;
    }

    public class MapView extends View {

        public MapView(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            Bitmap i = BitmapFactory.decodeResource(getResources(), R.drawable.zoomap);
            Affichage.afficher(this, canvas, i);
        }
    }*/
}
